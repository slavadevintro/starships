import Vue from 'vue'
import Router from 'vue-router'
import AppStarships from './views/AppStarships.vue'
import AppIdStarships from './views/AppIdStarships.vue'
import NotFound from './views/AppNotFound.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Starships',
      component: AppStarships
    },
    {
      path: '/:params',
      name: 'Search',
      component: AppStarships
    },
    {
      path: '/404',
      name: '404',
      component: NotFound,
    },
    {
      path: '/spaceship/:id',
      name: 'Spaceship',
      component: AppIdStarships
    }
  ]
})
